package tests;

import org.testng.annotations.Test;

import pages.instagram.EditUserProfilePage;
import pages.instagram.LoginPage;
import pages.instagram.MainMenuPage;
import pages.instagram.SearchResultPage;
import pages.instagram.UserProfilePage;

import static io.appium.java_client.MobileCommand.LAUNCH_APP;
import static org.testng.Assert.assertTrue;

public class InstagramTests extends AndroidBaseTest {

    @Test
    public void MainScreenWithSearchTest(){
        driver.execute(LAUNCH_APP);

        LoginPage loginPage = new LoginPage(driver);
        assertTrue(loginPage.isShown());
        loginPage.setEmailField();
        loginPage.setPasswordField();

        MainMenuPage mainMenuPage = loginPage.performLogin();
        assertTrue(mainMenuPage.isShown());

        SearchResultPage searchResultPage = mainMenuPage.performSearch();
        assertTrue(searchResultPage.isShown());
    }

    @Test
    public void EditProfilesTest(){
        driver.launchApp();

        LoginPage loginPage = new LoginPage(driver);
        assertTrue(loginPage.isShown());
        loginPage.setEmailField();
        loginPage.setPasswordField();

        MainMenuPage mainMenuPage = loginPage.performLogin();
        assertTrue(mainMenuPage.isShown());

        UserProfilePage UserPage = mainMenuPage.proceedToUserProfile();
        assertTrue(UserPage.isShown());

        EditUserProfilePage EditUserPage = UserPage.proceedToEdit();
        assertTrue(EditUserPage.isShown());
    }
}
