package pages.splitwise;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;
import pages.common.BasePage;

import org.openqa.selenium.WebElement;

public class LauncherPage extends BasePage {

  @AndroidFindBy(id = "com.Splitwise.SplitwiseMobile:id/signupButton" )
  private WebElement signUpButton;

  public LauncherPage(AndroidDriver driver){
    super(driver);
  }

  @Step("SignUp")
  public void signUp(){
    signUpButton.click();
  }
}
