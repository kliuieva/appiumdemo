## Android app with Appium

#### Requires:
Java 11
Appium Java client  >= 8.5.0
Running instance of Appium server >= 2.0 

#### Test Subject
Test application instagram.apk (added to /resources folder) and Splitwise.apk

#### Functionality covered
1. Splitwise.apk - create user, select currency, select phone numbers

#### How To Run
//TDB: to be updated
mvn clean test -Dappium=appium_url  (by default for local runs -Dappium=http://127.0.0.1:4723/)

default driver is UiAutomator2
