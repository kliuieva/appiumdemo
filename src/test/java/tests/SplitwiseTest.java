package tests;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Stories;
import io.qameta.allure.Story;
import pages.splitwise.LauncherPage;
import pages.splitwise.SignUpPage;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SplitwiseTest extends AndroidBaseTest {

  @Test
  @Description("Verify that user can't SignUp on emulator")
  @Severity(SeverityLevel.CRITICAL)
  public void signUpWithValidDataTest() {
    driver.launchApp();

    String excpectedErrorText = "An unknown error occurred. Please try again or contact support@splitwise.com if you experience repeated issues. Sorry for the trouble!";
    String fullName = "Test name";
    String validEmail = "testAddress@gmail.com";
    String validPassword = "Password123@";

    LauncherPage launcherPage = new LauncherPage(driver);
    launcherPage.signUp();

    SignUpPage signUpPage = new SignUpPage(driver);
    signUpPage.setFullName(fullName);

    signUpPage.setEmailAddress(validEmail);
    signUpPage.setPassword(validPassword);

    signUpPage.clickDoneButton();
    String actualErrorText = signUpPage.getErrorSignUpMessage();

    Assert.assertTrue(excpectedErrorText.equals(actualErrorText));
  }

  @Test
  public void signUpWithCurrency() throws InterruptedException {
    driver.launchApp();

    LauncherPage launcherPage = new LauncherPage(driver);
    Thread.sleep(1500);
    launcherPage.signUp();
    SignUpPage signUpPage = new SignUpPage(driver);
    signUpPage.setFullName("Barak Obama");
    Thread.sleep(1000);
    signUpPage.setEmailAddress("abc@gmail.com");
    signUpPage.setPassword("password123");
    signUpPage.tapCurrencyLink();
    signUpPage.selectDifferentCurrency("CHF – Swiss Franc (Fr.)");
    String actualCurrencyText = signUpPage.getCurrencyText();
    Assert.assertTrue(actualCurrencyText.equals("I use CHF (Fr.) as my currency. Change »"));
  }
}
