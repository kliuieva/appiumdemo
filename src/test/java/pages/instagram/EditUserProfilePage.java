package pages.instagram;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import pages.common.BasePage;

import java.util.stream.Stream;

import org.openqa.selenium.WebElement;

public class EditUserProfilePage extends BasePage {


    @AndroidFindBy(accessibility = "Back")
    private WebElement BackButton;

    public EditUserProfilePage(AndroidDriver driver){
        super(driver);
    }

    public boolean isShown() {
        return Stream.of(BackButton).allMatch(WebElement::isDisplayed);
    }

}
