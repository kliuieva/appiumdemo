package pages.splitwise;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Severity;
import io.qameta.allure.Step;

import java.time.Duration;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignUpPage {

  public AndroidDriver driver;

  public SignUpPage(AndroidDriver driver){
    this.driver = driver;
  }


  @Step("Set fullName {0}")
  public void setFullName(String name) {
    //FullName
    WebElement fullName = driver.findElement(By.id("com.Splitwise.SplitwiseMobile:id/nameField"));
    fullName.click();
    fullName.sendKeys(name);
  }

  @Step("Set email {0}")
  public void setEmailAddress(String email) {
    new WebDriverWait(driver, Duration.ofSeconds(10)).until(
        ExpectedConditions.elementToBeClickable(By.id("com.Splitwise.SplitwiseMobile:id/emailField2")));

    WebElement emailAddress = driver.findElement(
        By.id("com.Splitwise.SplitwiseMobile:id/emailField2"));
        emailAddress.click();
    emailAddress.sendKeys(email);
  }

  @Step("Set password {0}")
  public void setPassword(String passwordText) {
    WebElement password = driver.findElement(
        By.id("com.Splitwise.SplitwiseMobile:id/passwordFieldSignup"));
    password.click();
    password.sendKeys(passwordText);
  }

  @Step("Click Done")
  public void clickDoneButton() {
    WebElement doneButton = driver.findElement(
        By.id("com.Splitwise.SplitwiseMobile:id/doneButton"));
    doneButton.click();
  }

  public String getErrorSignUpMessage() {
    WebElement errorSignUpMessage = driver.findElement(
        By.id("com.Splitwise.SplitwiseMobile:id/content"));
    return errorSignUpMessage.getText();
  }

  public void tapCurrencyLink() {
    WebElement link = driver.findElement(
        By.id("com.Splitwise.SplitwiseMobile:id/currencyButton"));
    link.click();
  }

  public void selectDifferentCurrency(String currencyName) {
    By xpathToCurrency = By.xpath("//*[@text='" + currencyName + "']");
    scrollForMobile(xpathToCurrency);
    WebElement currencyValue = driver.findElement(xpathToCurrency);
    currencyValue.click();
  }

  public String getCurrencyText() {
    WebElement currencyText = driver.findElement(
        By.id("com.Splitwise.SplitwiseMobile:id/currencyButton"));
    return currencyText.getText();
  }

  public void scrollForMobile(By by) {
    String previousPageSource = "";
    while (!isElementEnabled(by) && isNotEndOfPage(previousPageSource)) {
      previousPageSource = driver.getPageSource();
      performScroll();
    }
  }

  public boolean isNotEndOfPage(String previousPageSource) {
    return !previousPageSource.equals(driver.getPageSource());
  }

  public boolean isElementEnabled(By by){
    try{
      WebElement elem = driver.findElement(by);
      return elem.isDisplayed();
    } catch (NoSuchElementException ex){
      return false;
    }
  }

  public void performScroll() {
    Dimension size = driver.manage().window().getSize();
    int startX = size.getWidth() / 2;
    int endX = size.getWidth() / 2;
    int startY = size.getHeight() / 2;
    int endY = (int) (size.getHeight() * 0.25);

    performScrollUsingSequence(startX, startY, endX, endY);
  }

  public void performScrollUsingSequence(int startX, int startY, int endX, int endY) {
    PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH, "first-finger");
    Sequence sequence = new Sequence(finger, 0)
        .addAction(finger.createPointerMove(Duration.ZERO, PointerInput.Origin.viewport(), startX, startY))
        .addAction(finger.createPointerDown(PointerInput.MouseButton.LEFT.asArg()))
        .addAction(finger.createPointerMove(Duration.ofMillis(300), PointerInput.Origin.viewport(), endX, endY))
        .addAction(finger.createPointerUp(PointerInput.MouseButton.LEFT.asArg()));

    ((driver)).perform(Collections.singletonList(sequence));
  }
}
