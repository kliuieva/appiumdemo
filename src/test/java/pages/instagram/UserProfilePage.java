package pages.instagram;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import pages.common.BasePage;

import java.util.stream.Stream;

import org.openqa.selenium.WebElement;

public class UserProfilePage extends BasePage {

    @AndroidFindBy(id = "com.instagram.android:id/row_profile_header_edit_profile")
    private WebElement EditProfileButton;

    public UserProfilePage(AndroidDriver driver){
        super(driver);
    }


    public boolean isShown() {
        return Stream.of(EditProfileButton).allMatch(WebElement::isDisplayed);
    }

    public EditUserProfilePage proceedToEdit(){
        EditProfileButton.click();
        return new EditUserProfilePage(driver);
    }
}
