package tests;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class AndroidBaseTest {

  public static final String APPIUM_URL = "http://127.0.0.1:4723/";//System.getProperty("appium");
  public static final String ANDROID_PLATFORM_NAME = "Android";
  public static final String TEST_APP = "instagram.apk";
  protected static AndroidDriver driver;

  Logger log = Logger.getLogger(AndroidBaseTest.class.getName());

  @BeforeClass
  public void setUp() {
    driver = initDriver();
  }

  @AfterClass
  public void tearDown() {
    if (driver != null) {
      driver.closeApp();
      driver.quit();
    }
  }

  @BeforeMethod
  public void setTestApp() {
    driver.resetApp();
  }

  private AndroidDriver initDriver() {
    try {
        //Local
      //driver = new AndroidDriver(new URL(APPIUM_URL), getLocalCapabilitiesForSplitwise());
      //log.info("Appium url: " + APPIUM_URL);
        String userName = "sdfghcvbn_Nvh0ca";
        String accessKey = "VA7KXZGvq96iVy39ptrD";

        driver = new AndroidDriver(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), getRemoteCapabilitiesForSplitwise());
    } catch (Exception ex) {
      throw new RuntimeException("Appium driver could not be initialized." + ex.getMessage());
    }
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    return driver;
  }

  private UiAutomator2Options getLocalCapabilitiesForInstagram() {
    UiAutomator2Options options = new UiAutomator2Options()
        .setApp(
            "/Users/anna.kl/Desktop/training/AppiumInstagramDemo/alevel/src/main/resources/instagram.apk")
        .setPlatformName(ANDROID_PLATFORM_NAME)
        .setAutomationName("uiautomator2")
        .setDeviceName("Pixel_6")
        .setPlatformVersion("11.0")
        .setAppPackage("com.instagram.android")
        .setAppActivity("com.instagram.mainactivity.MainActivity")
        .setNoReset(true);

    return options;
  }

  private UiAutomator2Options getLocalCapabilitiesForSplitwise() {
    UiAutomator2Options options = new UiAutomator2Options()
        .setApp("/Users/anna.kl/Desktop/demo/Demo/src/main/resources/splitwise.apk")
        .setPlatformName(ANDROID_PLATFORM_NAME)
        .setAutomationName("uiautomator2")
        .setDeviceName("Pixel_6")
        .setPlatformVersion("11.0")
        .setAppPackage("com.Splitwise.SplitwiseMobile")
        .setAppActivity("com.Splitwise.SplitwiseMobile.views.SplitwiseSplashScreen")
        .setNoReset(true);

    return options;
  }

  private UiAutomator2Options getRemoteCapabilitiesForSplitwise() {
    UiAutomator2Options options = new UiAutomator2Options()
        .setApp("bs://9d8a8bec86f4e86dcc680e22435357a911bf000f")
        .setPlatformName("android")
        .setAutomationName("uiautomator2")
        .setDeviceName("Google Pixel 6")
        .setPlatformVersion("12.0")
        .setAppPackage("com.Splitwise.SplitwiseMobile")
        .setAppActivity("com.Splitwise.SplitwiseMobile.views.SplitwiseSplashScreen")
        .setNoReset(true);

    HashMap<String, Object> browserstackOptions = new HashMap<String, Object>();
    browserstackOptions.put("appiumVersion", "2.0.0");
    browserstackOptions.put("debug", "true");

    options.setCapability("bstack:options", browserstackOptions);

    return options;
  }

}
