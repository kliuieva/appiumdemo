package pages.instagram;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import pages.common.BasePage;

import java.util.stream.Stream;

import org.openqa.selenium.WebElement;

public class MainMenuPage extends BasePage {

    @AndroidFindBy(accessibility = "Search and Explore")
    private WebElement SearchAndExploreButton;

    @AndroidFindBy(accessibility = "Home")
    private WebElement HomeButton;

    @AndroidFindBy(accessibility = "Profile")
    private WebElement ProfileButton;

    @AndroidFindBy(id = "com.instagram.android:id/tab_icon", accessibility = "Camera")
    private WebElement CameraButton;

    public MainMenuPage(AndroidDriver driver){
        super(driver);
    }

    public SearchResultPage performSearch(){
        SearchAndExploreButton.click();
        return new SearchResultPage(driver);
    }

    public UserProfilePage proceedToUserProfile(){
        ProfileButton.click();
        return new UserProfilePage(driver);
    }

    public boolean isShown() {
        return Stream.of(SearchAndExploreButton, HomeButton, CameraButton, ProfileButton).allMatch(WebElement::isDisplayed);
    }
}
