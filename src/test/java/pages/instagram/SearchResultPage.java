package pages.instagram;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import pages.common.BasePage;

import java.util.stream.Stream;

import org.openqa.selenium.WebElement;

public class SearchResultPage extends BasePage {

    @AndroidFindBy(xpath = "//*[@text=\"For You\"]")
    private WebElement ForYouFeedPage;

    public SearchResultPage(AndroidDriver driver){
        super(driver);
    }


    public boolean isShown() {
        return Stream.of(ForYouFeedPage).allMatch(WebElement::isDisplayed);
    }
}
