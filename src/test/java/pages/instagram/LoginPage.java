package pages.instagram;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import pages.common.BasePage;

import java.util.stream.Stream;

import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage {

    private String pwd = "Westy123";
    private String email = "justcasualuser5@gmail.com";

    @AndroidFindBy(xpath = "//android.view.View[@content-desc='Username, email or mobile number']/ancestor::android.view.ViewGroup/following-sibling::android.widget.EditText")
    private WebElement emailField;

    @AndroidFindBy(xpath = "//android.view.View[@content-desc='Password']/ancestor::android.view.ViewGroup/following-sibling::android.widget.EditText")
    private WebElement pwdField;

    @AndroidFindBy(xpath = "//android.widget.Button[@content-desc='Log in']")
    private WebElement ProceedLoginbutton;

    public LoginPage(AndroidDriver driver){
        super(driver);
    }

    public LoginPage setEmailField(){
        emailField.sendKeys(email);
        return this;
    }

    public LoginPage setPasswordField(){
        pwdField.sendKeys(pwd);
        return this;
    }

    public MainMenuPage performLogin(){
        ProceedLoginbutton.click();
        return new MainMenuPage(driver);
    }

    public boolean isShown() {
        return Stream.of(emailField, pwdField, ProceedLoginbutton).allMatch(WebElement::isDisplayed);
    }
}
